package formatter

func MakeCollapsible(content string) string {
	return "<details>\n<summary>Show</summary>\n\n" + content + "</details>\n"
}
