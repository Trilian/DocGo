package helper

import (
	"io/ioutil"
	"log"
	"os"
)

/*
@name:			ReadFile
@description:	ReadFile reads a file and returns the data as byte array.
				It handles errors.
@author:		Tristan
@param:			filename string: the name of the file to read
@return:		data string: the content of the file as byte array
*/
func ReadFile(filename string) []byte {
	filename = "./" + filename
	dat, err := os.ReadFile(filename)
	ErrCheck(err)
	return dat
}

/*
@name:			WriteFile
@description:	WriteFile writes data to a file
@author:		Tristan
@param:			filename string: the name of the file to write to
@param:			data string: the data to write to the file
@return:		none
*/
func WriteFile(filename string, data string) {
	//Write first line
	err := ioutil.WriteFile(filename, []byte(data+"\n"), 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func AppendFile(filename string, data string) {
	//Append second line
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()
	if _, err := file.WriteString(data + "\n\n"); err != nil {
		log.Fatal(err)
	}
}
