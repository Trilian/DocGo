package helper

import (
	"flag"
	"fmt"
)

func ReadArgs() {
	file := flag.String("file", "", "The file to read")

	flag.Parse()

	fmt.Println("file:", *file)
}
