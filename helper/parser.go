package helper

import (
	"regexp"
	"strings"

	"blockchainkids.de/docgo/structs"
)

func ParseDocBlock(b []string) structs.Block {

	docBlock := structs.Block{}
	parts := strings.Split(b[1], "@")

	for _, v := range parts {
		docBlockContent := structs.BlockContent{}
		if v == "" {
			continue
		}
		// regex to get header until :
		re := regexp.MustCompile(`(?m)^(.*?):`)
		header := re.FindStringSubmatch(v)[1]

		// regex to get content after :
		re = regexp.MustCompile(`(?m)^.*?:(.*)`)
		content := re.FindStringSubmatch(v)[1]
		content = CleanContent(content)

		switch header {
		case "name":
			docBlock.Header = content
		}

		docBlockContent.Name = strings.Title(header)
		docBlockContent.Value = content

		docBlock.Content = append(docBlock.Content, docBlockContent)
	}
	return docBlock
}

func CleanContent(content string) string {
	content = strings.TrimSpace(content)
	content = strings.ReplaceAll(content, "\n", " ")
	content = strings.ReplaceAll(content, "\t", "")
	return content
}
