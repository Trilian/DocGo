package helper

/*
@name:			ErrCheck
@description:	ErrCheck checks if an error occured and if so panics
@author:		Tristan
@param:			e - the error to ErrCheck
@return:		none
*/
func ErrCheck(e error) {
	if e != nil {
		panic(e)
	}
}
