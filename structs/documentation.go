package structs

type Documentation struct {
	Header    string
	DocBlocks []Block
}
