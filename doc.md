# Helper
## ReadFile

Description

<details>
<summary>Show</summary>

ReadFile reads a file and returns the data as byte array.</details>


|Author|
|:---|
|Tristan|

|Param|
|:---|
|filename string: the name of the file to read|

|Return|
|:---|
|data string: the content of the file as byte array|

## WriteFile

Description

<details>
<summary>Show</summary>

WriteFile writes data to a file</details>


|Author|
|:---|
|Tristan|

|Param|
|:---|
|filename string: the name of the file to write to|

|Param|
|:---|
|data string: the data to write to the file|

|Return|
|:---|
|none|

