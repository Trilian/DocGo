package main

import (
	"regexp"

	format "blockchainkids.de/docgo/formatter"
	"blockchainkids.de/docgo/helper"
	"blockchainkids.de/docgo/structs"
)

// for testing purposes
var outFile = "./doc.md"
var inFile = "./helper/iohelper.go"

func main() {
	documentation := structs.Documentation{Header: "Helper"}

	file := helper.ReadFile("./helper/rw.go")

	code := string(file)

	// regex to find the documentation starting with /* and ending with */
	re := regexp.MustCompile(`(?s)/\*\n(.*?)\n\*/`)

	// find all matches
	matches := re.FindAllStringSubmatch(code, -1)

	for _, v := range matches {
		documentation.DocBlocks = append(documentation.DocBlocks, helper.ParseDocBlock(v))
	}

	helper.WriteFile(outFile, "# "+documentation.Header)

	for _, db := range documentation.DocBlocks {
		helper.AppendFile(outFile, "## "+db.Header)
		for _, dc := range db.Content {
			if dc.Name == "Name" {
				continue
			}
			if dc.Name == "Author" || dc.Name == "Param" || dc.Name == "Return" {
				helper.AppendFile(outFile, "|"+dc.Name+"|\n"+"|:---|\n"+format.MakeTable(dc.Value))
				continue
			}
			helper.AppendFile(outFile, dc.Name)
			helper.AppendFile(outFile, format.MakeCollapsible(dc.Value))
		}
	}
}
